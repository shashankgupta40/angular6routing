import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { routingModule } from './app.routing';
import { Page1Component } from './page1/page1.component';
import { Page2Component } from './page2/page2.component';
import { Page3Component } from './page3/page3.component';
import { Page4Component } from './page4/page4.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { DisplayIdComponent } from './display-id/display-id.component';

@NgModule({
  declarations: [
    AppComponent, Page1Component, Page2Component, Page3Component, Page4Component, NotFoundComponent, DisplayIdComponent
  ],
  imports: [
    BrowserModule,
    routingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
