import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayIdComponent } from './display-id.component';

describe('DisplayIdComponent', () => {
  let component: DisplayIdComponent;
  let fixture: ComponentFixture<DisplayIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayIdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
