import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-display-id',
  templateUrl: './display-id.component.html',
  styleUrls: ['./display-id.component.css']
})
export class DisplayIdComponent implements OnInit {
  public id: number;

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.id = params['id'];
    });
  }

}
