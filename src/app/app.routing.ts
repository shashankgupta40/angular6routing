import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { Page1Component } from './page1/page1.component';
import { Page2Component } from './page2/page2.component';
import { Page3Component } from './page3/page3.component';
import { Page4Component } from './page4/page4.component';
import { DisplayIdComponent } from './display-id/display-id.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AppComponent } from './app.component';

const routes: Routes = [
    { path: 'learn/page1', component: Page1Component },
    { path: 'learn/page2', component: Page2Component, data: { title: 'Heroes List' } },
    { path: 'learn/page3', component: Page3Component },
    { path: 'learn/page4', component: Page4Component },
    { path: 'learn/display/:id', component: DisplayIdComponent },
    {
        path: '',
        redirectTo: 'learn/page1',
        pathMatch: 'full',
    },
    { path: '**', component: NotFoundComponent }
];

export const routingModule: ModuleWithProviders = RouterModule.forRoot(routes);
